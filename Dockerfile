FROM php:7.4-apache

# Composer version
ARG composer_ver=1.10.13
ARG composer_hash=5ca7445cfd48dd27c5a84aa005a47b4d9fd91132313830609875df3a6973708f

# save composer
ARG composer_path=/usr/local/bin/composer

# enable apache rewrite
RUN rm /etc/apache2/sites-available/000-default.conf
COPY ./config/apache/default.conf /etc/apache2/sites-available/000-default.conf


RUN apt-get update \
  # install composer
  && php -r "copy('https://getcomposer.org/download/$composer_ver/composer.phar', '$composer_path');" \
  && chmod 755 $composer_path \
  && php -r "if (hash_file('sha256', '$composer_path') !== '$composer_hash') { \
          echo '!!! Failed to install Composer !!!'; \
          unlink('$composer_path'); \
      } \
      echo PHP_EOL;" \
  && apt-get install -y zip \
  && apt-get install -y unzip \
  # PHP intl extension (CakePHP require)
  && apt-get install -y libicu-dev \
  && docker-php-ext-install intl \
  # install git
  && apt-get install -y git \
  # install MySQL
  && apt-get install -y libmariadb-dev && docker-php-ext-install pdo_mysql \
  # enable mod_rewrite
  && a2enmod rewrite

RUN apt-get install -y libpng-dev
RUN apt-get install -y libzip-dev
RUN apt-get install -y zlib1g-dev
RUN docker-php-ext-install gd
RUN docker-php-ext-install zip
